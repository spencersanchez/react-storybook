import React from 'react'
import { storiesOf } from '@kadira/storybook'
import { withKnobs, boolean, color, object, number, text } from '@kadira/storybook-addon-knobs'

import PurchLogo from '../components/PurchLogo'
import Txt from '../components/Txt'
import ProfilePic from '../components/ProfilePic'
import ProfileCard from '../components/ProfileCard'
import Checkbox from '../components/Checkbox'
// import ReactLogo from '../components/ReactLogo'

const storiesPurchLogo = storiesOf('PurchLogo', module)
storiesPurchLogo.addDecorator(withKnobs)
storiesPurchLogo
	.addWithInfo(
		'default',
		`
			Purch standard logo, color fill
		`,
		() => <PurchLogo />
	)
	.addWithInfo(
		'boxed',
		`
			Purch standard logo, boxed
		`,
		() => {
			const styles = {
				backgroundColor: '#e04622',
				padding: '2em',
				width: '300px'
			}
			return <PurchLogo fill='#fff' styles={styles} />
		}
	)
	.addWithInfo(
		'sandbox',
		`
			Purch standard logo with changable fill color and wrapper styles
		`,
		() => {
			const fill = color('Fill color', '#fff')
			const styles = object('Wrapper styles', {
				backgroundColor: '#e04622',
				padding: '2em',
				width: '300px'
			})
			return <PurchLogo fill={fill} styles={styles} />
		}
	)
	.addWithInfo(
		'styleguide page',
		`
			This is the Purch standard logo styleguide page.
		`,
		() => <PurchLogo />,
		{ inline: true }
	)

const storiesTxt = storiesOf('Txt', module)
storiesTxt.addDecorator(withKnobs)
storiesTxt
	.addWithInfo(
		'default',
		`
			Reusable text component, default
		`,
		() => <Txt />
	)
	.addWithInfo(
		'headline',
		`
			Sample headline style
		`,
		() => {
			const styles = {
				"fontFamily": "Avenir, sans-serif",
				"fontSize": "3rem",
				"fontWeight": "bold",
				"textTransform": "uppercase"
			}
			return <Txt txt='Hear ye, hear ye!' styles={styles} />
		}
	)
	.addWithInfo(
		'sandbox',
		`
			Sample headline style
		`,
		() => {
			const txt = text('Content', "Heard you!!!")
			const styles = object('Styles', {
				"fontFamily": "Avenir, sans-serif",
				"fontSize": "6rem",
				"fontWeight": "bolder",
				"textTransform": "uppercase"
			})
			return <Txt txt={txt} styles={styles} />
		}
	)
	.addWithInfo(
		'styleguide page',
		`
			Text component styleguide page.
		`,
		() => <Txt />,
		{ inline: true }
	)

// get a random person
let people = {}
let person = {}
let request = new XMLHttpRequest()
request.open('GET', 'https://randomuser.me/api/?nat=us', true)
request.onload = function() {
	if (request.status >= 200 && request.status < 400) {
		people = JSON.parse(request.response)
		person = people.results[0]
	} else {
		console.error('Page loaded though bad status.')
	}
}
request.onerror = function() {
	console.error("Couldn't get a random person")
}
request.send()


const storiesProfilePic = storiesOf('ProfilePic', module)
storiesProfilePic.addDecorator(withKnobs)
storiesProfilePic
	.addWithInfo(
		'default',
		`
			Profile picture of user, default.
		`,
		() => <ProfilePic />
	)
	.addWithInfo(
		'color sample',
		`
			Profile picture of user, sample person.
		`,
		() => {
			const alt = text('Alt text', `${person.name.first} ${person.name.last}`)
			const isDisabled = boolean('Disabled', false)
			const src = text('Src', person.picture.large)
			const styles = object('Styles', {
				inner: {},
				outer: {}
			})
			return (<ProfilePic alt={alt} isDisabled={isDisabled} src={src} styles={styles} />)
		}
	)
	.addWithInfo(
		'disabled sample',
		`
			Profile picture of user, disabled
		`,
		() => {
			const alt = `${person.name.first} ${person.name.last}`
			const src = person.picture.large
			const styles = {
				inner: {},
				outer: {}
			}
			return <ProfilePic alt={alt} isDisabled={true} src={src} styles={styles} />
		}
	)
	.addWithInfo(
		'sandbox',
		`
			Profile picture of user, configurable alt text, disabled state, src image, and styles.
		`,
		() => {
			const alt = text('Alt text', `${person.name.first} ${person.name.last}`)
			const isDisabled = boolean('Disabled', false)
			const src = text('Src', person.picture.large)
			const styles = object('Styles', {
				inner: {},
				outer: {}
			})
			return <ProfilePic alt={alt} isDisabled={isDisabled} src={src} styles={styles} />
		}
	)
	.addWithInfo(
		'styleguide page',
		`
			Profile picture of user styleguide page.
		`,
		() => <ProfilePic />,
		{ inline: true }
	)


const storiesProfileCard = storiesOf('ProfileCard', module)
storiesProfileCard.addDecorator(withKnobs)
storiesProfileCard
	.addWithInfo(
		'default',
		`
			Profile card for a user, default.
		`,
		() => <ProfileCard />
	)
	.addWithInfo(
		'color sample',
		`
			Profile card for a user, sample person.
		`,
		() => {
			const nme = `${person.name.first} ${person.name.last}`
			const src = person.picture.large
			const styles = {
				card: {},
				nme: {},
				pic: {
					inner: {},
					outer: {}
				},
				title: {}
			}
			return <ProfileCard nme={nme} src={src} styles={styles} title='Manager' />
		}
	)
	.addWithInfo(
		'disabled sample',
		`
			Profile card for a user, disabled
		`,
		() => {
			const nme = `${person.name.first} ${person.name.last}`
			const src = person.picture.large
			const styles = {
				card: {},
				nme: {},
				pic: {
					inner: {},
					outer: {}
				},
				title: {}
			}
			return (<ProfileCard nme={nme} isDisabled={true} src={src} styles={styles} title='Manager' />)
		}
	)
	.addWithInfo(
		'sandbox',
		`
			Profile card for a user, configurable name, title, disabled state, source image, and styles
		`,
		() => {
			const nme = text('Name', `${person.name.first} ${person.name.last}`)
			const title = text('Title', 'Senior Boss')
			const isDisabled = boolean('Disabled', false)
			const src = text('Src', person.picture.large)
			const styles = object('Styles', {
				card: {},
				nme: {},
				pic: {
					inner: {},
					outer: {}
				},
				title: {}
			})
			return <ProfileCard nme={nme} isDisabled={isDisabled} src={src} styles={styles} title={title} />
		}
	)
	.addWithInfo(
		'styleguide page',
		`
			Profile card for a user styleguide page.
		`,
		() => <ProfileCard />,
		{ inline: true }
	)

const storiesCheckbox = storiesOf('Checkbox', module)
storiesCheckbox.addDecorator(withKnobs)
storiesCheckbox
	.addWithInfo(
		'default',
		`
			Checkbox, default (unchecked)
		`,
		() => <Checkbox />
	)
	.addWithInfo(
		'checked',
		`
			Checkbox, checked (and styled a bit)
		`,
		() => {
			const styles = {
				height: '50px',
				fill: '#5a6ca3',
				width: '50px'
			}
			return <Checkbox isChecked={true} styles={styles} />
		}
	)
	.addWithInfo(
		'sandbox',
		`
			Checkbox with configurable checked state, fill color, and size
		`,
		() => {
			const isChecked = boolean('Checked', true)
			const fillColor = color('Fill', '#5a6ca3')
			const size = number('Size', 50, {
				range: true,
				min: 10,
				max: 100,
				step: 1
			})
			const styles = object('Styles', {
				height: `${size}px`,
				fill: fillColor,
				width: `${size}px`
			})
			return <Checkbox isChecked={isChecked} styles={styles} />
		}
	)
	.addWithInfo(
		'styleguide page',
		`
			Checkbox styleguide page.
		`,
		() => <Checkbox />,
		{ inline: true }
	)

// const storiesReactLogo = storiesOf('ReactLogo', module)
// storiesReactLogo.addDecorator(withKnobs)
// storiesReactLogo
// 	.addWithInfo(
// 		'default',
// 		`
// 			React logo
// 		`,
// 		() => <ReactLogo />
// 	)
