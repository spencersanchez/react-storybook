import React from 'react'
import ReactDOM from 'react-dom'
import ReactLogo from './ReactLogo'

it('renders without crashing', () => {
	const div = document.createElement('div')
	ReactDOM.render(<ReactLogo />, div)
})

