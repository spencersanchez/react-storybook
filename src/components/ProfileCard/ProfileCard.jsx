// @flow
import React, { Component, PropTypes } from 'react'
import Txt from '../Txt'
import ProfilePic from '../ProfilePic'

class ProfileCard extends Component {
	render() {
		const baseTextStyles = {
			color: this.props.isDisabled ? '#999' : '#000',
			display: 'inline-block',
			fontFamily: 'Lucinda Grande, sans-serif',
			paddingLeft: '1em',
			textTransform: 'capitalize',
			verticalAlign: 'middle'
		}

		const cardStyles = Object.assign({}, {
			backgroundColor: '#dee2e4',
			borderRadius: '10px',
			display: 'inline-block',
			padding: '15px'
		}, this.props.styles.card, this.props.isDisabled ? { backgroundColor: '#fff' } : {})

		const nmeStyles = Object.assign({}, {
			fontSize: '1.2rem'
		}, this.props.styles.nme)

		const titleStyles = Object.assign({}, {
			fontSize: '0.8rem',
			fontStyle: 'italic'
		}, this.props.styles.title)

		const picStyles = {
			inner: this.props.styles.pic ? this.props.styles.pic.inner : {},
			outer: Object.assign({},
				{
					border: this.props.isDisabled ? '2px solid #999' : '2px solid #000',
					backgroundColor: '#fff',
					display: 'inline-block',
					height: '100px',
					verticalAlign: 'middle',
					width: '100px'
				}, this.props.styles.pic ? this.props.styles.pic.outer : {})
		}


		return (
			<div style={cardStyles}>
				<ProfilePic
					src={this.props.src}
					styles={picStyles}
					alt={this.props.nme}
					isDisabled={this.props.isDisabled}
				/>
				<div style={baseTextStyles}>
					<Txt txt={this.props.nme} styles={nmeStyles} /><br />
					<Txt txt={this.props.title} styles={titleStyles} />
				</div>
			</div>
		)
	}
}

ProfileCard.propTypes = {
	isDisabled: PropTypes.bool,
	nme: PropTypes.string,
	src: PropTypes.string,
	styles: PropTypes.shape({
		card: PropTypes.object,
		nme: PropTypes.object,
		pic: PropTypes.object,
		title: PropTypes.object
	}),
	title: PropTypes.string
}

ProfileCard.defaultProps = {
	styles: {}
}

export default ProfileCard

