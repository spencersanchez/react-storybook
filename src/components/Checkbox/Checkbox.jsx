// @flow
import React, { Component, PropTypes } from 'react'

class Checkbox extends Component {
	render() {
		return this.props.isChecked ?
			(
				<svg viewBox="0 0 26 26" style={this.props.styles}>
					<path d="M19.4,4.9v16.3H2.8V4.9H19.4 M22.3,2H0V24h22.3V2L22.3,2z" />
					<polygon points="8.9,11.4 11.1,15.1 24.1,2 25.9,3 11.1,20.9 5.6,11.7" />
				</svg>
			)
			: (
				<svg viewBox="0 0 26 26" style={this.props.styles}>
					<path d="M19.4,4.9v16.3H2.8V4.9H19.4 M22.3,2H0V24h22.3V2L22.3,2z"/>
				</svg>
			)
	}
}

Checkbox.propTypes = {
	isChecked: PropTypes.bool,
	styles: PropTypes.object
}

Checkbox.defaultProps = {
	isChecked: false
}

export default Checkbox

