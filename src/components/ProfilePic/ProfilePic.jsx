// @flow
import React, { Component, PropTypes } from 'react'

class ProfilePic extends Component {
	render() {
		const innerStyles = Object.assign({}, {
			display: 'inline',
			height: '100%',
			margin: '0 auto',
			width: 'auto'
		}, this.props.styles.inner, this.props.isDisabled ? { filter: 'grayscale(100%)' } : {})

		const outerStyles = Object.assign({}, {
			borderRadius: '50%',
			height: '100px',
			overflow: 'hidden',
			position: 'relative',
			width: '100px'
		}, this.props.styles.outer)

		return (
			<div style={outerStyles}>
				<img src={this.props.src} alt={this.props.alt} style={innerStyles} />
			</div>
		)
	}
}

ProfilePic.propTypes = {
	alt: PropTypes.string,
	isDisabled: PropTypes.bool,
	src: PropTypes.string,
	styles: PropTypes.shape({
		inner: PropTypes.object,
		outer: PropTypes.object
	})
}

ProfilePic.defaultProps = {
	alt: 'Someone',
	isDisabled: false,
	src: 'https://upload.wikimedia.org/wikipedia/commons/a/ad/Placeholder_no_text.svg',
	styles: {}
}

export default ProfilePic

