// @flow
import React, { Component, PropTypes } from 'react'

class Txt extends Component {
	render() {
		return (
			<span style={this.props.styles}>{this.props.txt}</span>
		)
	}
}

Txt.propTypes = {
	styles: PropTypes.object,
	txt: PropTypes.string
}

Txt.defaultProps = {
	txt: "90% of the game is half mental"
}

export default Txt

