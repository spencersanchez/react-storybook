// @flow
import React, { Component, PropTypes } from 'react'

class PurchLogo extends Component {
	render() {
		return (
			<div style={this.props.styles}>
				<svg viewBox="101 -14 250 77" fill={this.props.fill}>
					<path d="M125-13.6h-24V1.1h22.9c9.7,0,14.1,3.7,14.1,10.3s-4.4,11.1-14.1,11.1h-23v38.2h14.4V37.3h9.7
						c16.6,0,27.6-10.9,27.6-25.4C152.6-2.7,141.6-13.6,125-13.6"/>
					<path d="M273.6,49.8c-8.2,0-15.4-6-15.4-15.2c0-8.4,6.6-15.2,14.7-15.2c5.1,0,9.4,2.3,13.2,7.8l10.6-7.7
						c-7.1-8.7-15.4-12.1-23.8-12.1C257,7.3,245,19.2,245,34.5c0,16.3,12.7,27.2,28.6,27.2c8.4,0,16.7-3.4,23.8-12.1L287,42.3
						C283.2,47.5,278.7,49.8,273.6,49.8"/>
					<path d="M328.9,7.6c-5.9,0-10.8,2.4-13.1,5.4v-26.6h-14.4v74.4h14.4V34.6c0-9.7,4.7-13.7,11.2-13.7
						c6.6,0,9.5,4.7,9.5,13v26.9H351V30.3C351,17.3,343.3,7.6,328.9,7.6"/>
					<path d="M192,37.3c0,9.3-4.1,13-10.5,13c-6.5,0-10.3-3.9-10.3-13V8.1h-14.4v31.2c0,15.3,9.4,22.6,24.1,22.6
						c4.9,0,8.6-1.4,11.1-4.1v3h14.4V8.1H192V37.3z"/>
					<path d="M227.1,11V8h-14.4v10v42.7h14.4V33.1c0-3.3-0.2-7.4,1.4-9.7c2.4-3.5,6.6-3.9,10.2-3.9c1.6,0,3,0.2,4.5,0.7
						c0.3,0.1,0.5,0.2,0.8,0.3l1.6-11.6c-2.3-0.6-3.3-1.3-7.7-1.3C231.2,7.5,227.5,10.5,227.1,11"/>
				</svg>
			</div>
		)
	}
}

PurchLogo.propTypes = {
	fill: PropTypes.string,
	styles: PropTypes.object
}

PurchLogo.defaultProps = {
	fill: '#e04622'
}

export default PurchLogo

