// @flow
import React, { Component } from 'react'

import PurchLogo from './components/PurchLogo'
import Txt from './components/Txt'
import ProfileCard from './components/ProfileCard'
import Checkbox from './components/Checkbox'
// import ReactLogo from './components/ReactLogo'

const styles = {
	wrapper: {
		margin: '10px auto',
		width: '800px'
	},
	logo: {
		backgroundColor: '#e04622',
		margin: '0 auto',
		padding: '2em',
		width: '400px'
	},
	headline:{
		display: 'block',
		fontFamily: 'Avenir, sans-serif',
		fontSize: '3rem',
		fontWeight: 'bold',
		textAlign: 'center',
		textTransform: 'capitalize'
	},
	card: {
		margin: '20px',
		width: '330px'
	},
	checkbox: {
		cursor: 'pointer',
		height: '20px',
		fill: '#5a6ca3',
		width: '20px',
		verticalAlign: 'middle'
	},
	label: {
		cursor: 'pointer',
		fontSize: '1.1rem',
		marginLeft: '0.5rem',
		verticalAlign: 'middle'
	},
	react: {
		display: 'inline-block',
		height: 'auto',
		verticalAlign: 'middle',
		width: '80px'
	}
}

// some titles for the demo scrum team members
const titles = [
	'Product Owner',
	'Scrum Master',
	'Business Analyst',
	'Quality Assurance Analyst',
	'Front End Engineer',
	'Front End Engineer',
	'Back End Engineer',
	'Back End Engineer',
	'UI Designer',
	'UX Professional'
]


class App extends Component {
	constructor(props) {
		super(props)
		this.handleClick = this.handleClick.bind(this)
		this.state = {
			isChecked: false,
			people: []
		}
	}

	// API call for some bunky users
	componentDidMount() {
		const self = this
		let request = new XMLHttpRequest()
		request.open('GET', 'https://randomuser.me/api/?nat=us&results=10', true)
		request.onload = function() {
			if (request.status >= 200 && request.status < 400) {
				self.setState({
					people: JSON.parse(request.response).results
				})
			} else {
				console.error('Page loaded though bad status.')
			}
		}
		request.onerror = function() {
			console.error("Couldn't get a random person")
		}
		request.send()
	}

	// Toggle the "only show people in this office" bool
	handleClick() {
		this.setState({
			isChecked: !this.state.isChecked
		})
	}

				// <div>
				// 	<ReactLogo styles={styles.react} />
				// 	<Txt txt='We LOVE React!!!' styles={styles.label} />
				// </div>

	render() {
		return (
			<div style={styles.wrapper}>
				<PurchLogo fill='#fff' styles={styles.logo} />
				<Txt txt='scrum team ultra' styles={styles.headline} />
				<div style={{ textAlign: 'center' }} onClick={this.handleClick}>
					<Checkbox isChecked={this.state.isChecked} styles={styles.checkbox} />
					<Txt txt='Only highlight team members at this office' styles={styles.label} />
				</div>
				{this.state.people.map((person, i) => {
					return (
						<ProfileCard
							nme={`${person.name.first} ${person.name.last}`}
							src={person.picture.large}
							title={titles[i]}
							styles={{card:styles.card}}
							isDisabled={this.state.isChecked && person.registered < '2008'}
							key={person.login.sha1}
						/>
					)
				})}
			</div>
		)
	}
}

export default App
